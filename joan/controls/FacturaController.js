'use strict';
const { validationResult } = require('express-validator');
var models = require('../models/');
var persona = models.persona;
var factura = models.factura;
var auto = models.auto;

class FacturaController {
    async listar(req, res) {
        var lista = await factura.findAll({
            include: { model: models.detalle, as: "detalle", attributes: ['subtotal'], include: { model: models.auto, foreignKey: 'id_auto', attributes: ['modelo', 'anio', 'cilindraje', 'color', 'precio', 'external_id'], include: { model: models.marca, foreignKey: 'id_marca', attributes: ['nombre', 'pais_origen'] } }},
            attributes: ['external_id', 'createdAt']
        });
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await factura.findOne({
            where: { external_id: external },
            include: { model: models.detalle, as: "detalle", attributes: ['subtotal'], include: { model: models.auto, foreignKey: 'id_auto', attributes: ['modelo', 'anio', 'cilindraje', 'color', 'precio', 'external_id'], include: { model: models.marca, foreignKey: 'id_marca', attributes: ['nombre', 'pais_origen'] } }},
            attributes: ['external_id', 'createdAt']
        });
        if (lista == null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let persona_id = req.body.external_persona;
            if (persona_id != undefined) {
                let personaAux = await persona.findOne({ where: { external_id: persona_id } });
                console.log(personaAux);
                if (personaAux) {
                    //var uuid = require('uuid');
                    //var external_factura = uuid.v4();
                    var lista = await factura.findAll();
                    var factura_id = lista.length + 1;

                    const detalle = [];
                    const detalleAux = req.body.detalle_factura;
                    const n = detalleAux.length;

                    for (let i = 0; i < n; i++) {
                        var auto_id = detalleAux[i].external_auto;
                        var autoAux = await auto.findOne({ where: { external_id: auto_id } });

                        const objeto = {
                            id_auto: autoAux.id,
                            id_factura: factura_id,
                            subtotal: autoAux.precio
                        };
                        auto.update(
                            { Duenio: personaAux.identificacion }, // Valor actualizado
                            { where: { id: autoAux.id } } // Condiciones de búsqueda
                          )

                        detalle.push(objeto);
                    }

                    var data = {
                        //id: factura_id,
                        id_persona: personaAux.id,
                        detalle: detalle
                    };
                    
                    console.log(data);
                    res.status(200);
                    
                    let transaction = await models.sequelize.transaction();
                    try {
                        await factura.create(data, { include: [{ model: models.detalle, as: "detalle" }], transaction });
                        await transaction.commit();
                        res.json({ msg: "Se ha registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }

                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }

            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
}

module.exports = FacturaController;