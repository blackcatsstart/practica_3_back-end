'use strict';
const { validationResult } = require('express-validator');
var models = require('../models/');
var marca = models.marca;

class MarcaController {
    async listar(req, res) {
        var lista = await marca.findAll({
            attributes: ['nombre', 'pais_origen', 'external_id']
        });
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async cantMarcas(req, res) {
        var cant = await marca.count();
        res.json({ msg: "OK!", code: 200, info: cant });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var data = {
                nombre: req.body.nombre,
                pais_origen: req.body.pais_origen,
            };
            res.status(200);
            //let transaction = await models.sequelize.transaction();
            try {
                await marca.create(data);
                //await transaction.commit();
                res.json({ msg: "Se ha registrado sus datos", code: 200 });
            } catch (error) {
                if (transaction) await transaction.rollback();
                if (error.errors && error.errors[0].message) {
                    res.json({ msg: error.errors[0].message, code: 200 });
                } else {
                    res.json({ msg: error.message, code: 200 });
                }
            }
        }
    }
}

module.exports = MarcaController;