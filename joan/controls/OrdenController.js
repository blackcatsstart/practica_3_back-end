'use strict';
const { validationResult } = require('express-validator');
var models = require('../models');
var persona = models.persona;
var factura = models.factura;

var orden_Trabajo = models.orden_Trabajo;
var repuesto = models.repuesto;
var auto = models.auto;

class OrdenController {

    async listar(req, res) {
        try {
            const lista = await orden_Trabajo.findAll({
                include: [
                    {
                        model: models.auto,
                        foreignKey: 'id_auto',
                        attributes: ['modelo', 'anio', 'cilindraje', 'color', 'duenio'],
                        include: [
                            {
                                model: models.marca,
                                foreignKey: 'id_marca',
                                attributes: ['nombre']
                            }
                        ]
                    },
                    {
                        model: models.detalle_orden,
                        as: 'detalle_orden',
                        attributes: ['cantidad', 'importe'],
                        include: [
                            {
                                model: models.repuesto,
                                foreignKey: 'id_repuesto',
                                attributes: ['external_id', 'descripcion', 'unidad', 'precio']
                            }
                        ]
                    }
                ],
                attributes: ['external_id', 'createdAt', 'fecha_Salida']
            });

            res.json({ msg: 'OK!', code: 200, info: lista });
        } catch (error) {
            console.error('Error al obtener la lista:', error);
            res.json({ msg: 'Error al obtener la lista', code: 500 });
        }
    }

    async obtener(req, res) {
        const external = req.params.external;
        try {
            const lista = await orden_Trabajo.findAll({
                where: { external_id: external },
                include: [
                    {
                        model: models.auto,
                        foreignKey: 'id_auto',
                        attributes: ['modelo', 'anio', 'cilindraje', 'color', 'duenio'],
                        include: [
                            {
                                model: models.marca,
                                foreignKey: 'id_marca',
                                attributes: ['nombre']
                            }
                        ]
                    },
                    {
                        model: models.detalle_orden,
                        as: 'detalle_orden',
                        attributes: ['cantidad', 'importe'],
                        include: [
                            {
                                model: models.repuesto,
                                foreignKey: 'id_repuesto',
                                attributes: ['external_id', 'descripcion', 'unidad', 'precio']
                            }
                        ]
                    }
                ],
                attributes: ['external_id', 'createdAt', 'fecha_Salida']
            });

            res.json({ msg: 'OK!', code: 200, info: lista });
        } catch (error) {
            console.error('Error al obtener la lista:', error);
            res.json({ msg: 'Error al obtener la lista', code: 500 });
        }
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let auto_id = req.body.external_auto;
            if (auto_id != undefined) {
                let autoAux = await auto.findOne({ where: { external_id: auto_id } });
                console.log(autoAux);
                if (autoAux) {
                    //var uuid = require('uuid');
                    //var external_factura = uuid.v4();
                    var lista = await orden_Trabajo.findAll();
                    var orden_id = lista.length + 1;

                    const detalle = [];
                    const detalleAux = req.body.detalle_orden;
                    const n = detalleAux.length;

                    for (let i = 0; i < n; i++) {
                        var repuesto_id = detalleAux[i].external_repuesto;
                        var cantidad = detalleAux[i].cantidad;
                        var repuestoAux = await repuesto.findOne({ where: { external_id: repuesto_id } });
                        var importe = cantidad * repuestoAux.precio;

                        const objeto = {
                            id_repuesto: repuestoAux.id,
                            id_orden: orden_id,
                            cantidad: cantidad,
                            importe: importe
                        };
                        detalle.push(objeto);
                    }

                    console.log(detalle);

                    var data = {
                        //id: factura_id,
                        id_auto: autoAux.id,
                        fecha_Salida: req.body.fecha_Salida,
                        detalle_orden: detalle
                    };

                    console.log(data);
                    res.status(200);

                    let transaction = await models.sequelize.transaction();
                    try {
                        await orden_Trabajo.create(data, { include: [{ model: models.detalle_orden, as: "detalle_orden" }], transaction });
                        await transaction.commit();
                        res.json({ msg: "Se ha registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }

                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }

            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
}

module.exports = OrdenController;