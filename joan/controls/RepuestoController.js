'use strict';
const { validationResult } = require('express-validator');
var models = require('../models/');
var marca = models.marca;

var repuesto = models.repuesto;

class RepuestoController {
    async listar(req, res) {
        var lista = await repuesto.findAll({
            attributes: ['descripcion', 'anio', 'unidad', 'precio', 'external_id']
        });
        res.json({ msg: "OK!", code: 200, info: lista });
    }

    async cantRepuesto(req, res) {
        var cant = await marca.count();
        res.json({ msg: "OK!", code: 200, info: cant });
    }

    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var data = {
                nombre: req.body.nombre,
                pais_origen: req.body.pais_origen,
            };
            res.status(200);
            //let transaction = await models.sequelize.transaction();
            try {
                await marca.create(data);
                //await transaction.commit();
                res.json({ msg: "Se ha registrado sus datos", code: 200 });
            } catch (error) {
                if (transaction) await transaction.rollback();
                if (error.errors && error.errors[0].message) {
                    res.json({ msg: error.errors[0].message, code: 200 });
                } else {
                    res.json({ msg: error.message, code: 200 });
                }
            }
        }
    }
}

module.exports = RepuestoController;