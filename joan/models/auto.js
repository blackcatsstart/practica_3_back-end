'use strict';
module.exports = (sequalize, DataTypes) => {
    const auto = sequalize.define('auto', {
        modelo: { type: DataTypes.STRING(100), defaultValue: "Sin_Datos" },
        anio: { type: DataTypes.INTEGER, defaultValue: 0 },
        cilindraje: { type: DataTypes.INTEGER, defaultValue: 0 },
        color: { type: DataTypes.ENUM('BLANCO', 'ROJO', 'NEGRO', 'AZUL', 'VERDE', 'MORADO', 'PLATA', 'MULTICOLOR'), defaultValue: 'BLANCO' },
        precio: { type: DataTypes.DECIMAL(10, 2), defaultValue: 0.0 },
        Duenio: {type: DataTypes.STRING(20), defaultValue: "Sin_Duenio"},
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    }, { freezeTableName: true });
    auto.associate = function (models) {
        auto.belongsTo(models.marca, { foreignKey: 'id_marca' });
        //auto.belongsTo(models.detalle, {foreignKey: 'id_detalle'});
        auto.hasOne(models.detalle, {foreignKey: 'id_auto', as: 'detalle'});
        //agregado recien
        auto.hasMany(models.orden_Trabajo, {foreignKey: 'id_auto', as: 'detalle_orden'});
    }
    return auto;
};