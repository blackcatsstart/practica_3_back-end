'use strict';
module.exports = (sequalize, DataTypes) => {
    const cuenta = sequalize.define('cuenta', {
        correo: {type: DataTypes.STRING(100), defaultValue: "Sin_Datos", unique: true},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        clave: {type: DataTypes.STRING(120), allowNull: false},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true}
    }, {freezeTableName: true});
    cuenta.associate = function(models) {
        cuenta.belongsTo(models.persona, {foreignKey: 'id_persona'})
    }
    return cuenta;
};