'use strict';
module.exports = (sequalize, DataTypes) => {
    const detalle_orden = sequalize.define('detalle_orden', {
        importe: { type: DataTypes.DECIMAL(10, 2), defaultValue: 0.0 },
        cantidad: { type: DataTypes.INTEGER, defaultValue: 1 },
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
    }, { freezeTableName: true });
    detalle_orden.associate = function (models) {
        detalle_orden.belongsTo(models.orden_Trabajo, {foreignKey: 'id_orden'});
        //detalle.hasOne(models.auto, {foreignKey: 'id_detalle', as: 'auto'});
        detalle_orden.belongsTo(models.repuesto, {foreignKey: 'id_repuesto'});
    }
    return detalle_orden;
};