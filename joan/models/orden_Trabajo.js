'use strict';
module.exports = (sequalize, DataTypes) => {
    const orden_Trabajo = sequalize.define('orden_Trabajo', {
        fecha_Salida: { type: DataTypes.DATE, allowNull: false},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4}
    }, { freezeTableName: true });
    orden_Trabajo.associate = function (models) {
        orden_Trabajo.belongsTo(models.auto, {foreignKey: 'id_auto'});
        orden_Trabajo.hasMany(models.detalle_orden, {foreignKey: 'id_orden', as: 'detalle_orden'});
    }
    return orden_Trabajo;
};