'use strict';
module.exports = (sequalize, DataTypes) => {
    const repuesto = sequalize.define('repuesto', {
        descripcion: { type: DataTypes.STRING(100), defaultValue: "Sin_Datos" },
        anio: { type: DataTypes.INTEGER, defaultValue: 0 },
        unidad: { type: DataTypes.STRING(20), defaultValue: "UNIDAD" },
        precio: { type: DataTypes.DECIMAL(10, 2), defaultValue: 0.0 },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    }, { freezeTableName: true });
    repuesto.associate = function (models) {
        //auto.belongsTo(models.detalle, {foreignKey: 'id_detalle'});
        repuesto.hasOne(models.detalle_orden, {foreignKey: 'id_repuesto', as: 'detalle_orden'});
    }
    return repuesto;
};